/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils4.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 11:23:10 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 11:23:11 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int				is_modif(int i)
{
	if ((i == 'h' && i + 1 == 'h') || i == 'h' || (i == 'l' && i + 1 == 'l')
		|| i == 'l' || i == 'j' || i == 'z')
		return (1);
	else
		return (0);
}

char			*lower_str(char *str)
{
	int			i;

	i = 0;
	while (str[i])
	{
		str[i] = ft_tolower(str[i]);
		i++;
	}
	return (str);
}

unsigned int	count_len(unsigned int n)
{
	int			i;

	i = 0;
	while (n)
	{
		n = n >> 1;
		i++;
	}
	return (i);
}

char			*wstr_convert(wchar_t *wstr)
{
	char		*str;
	unsigned	i;

	i = 0;
	if (wstr == NULL)
		return (ft_strdup("(null)"));
	str = ft_strnew(0);
	while (wstr[i])
	{
		str = ft_strjoin(str, w_convert(wstr[i]));
		i++;
	}
	return (str);
}

int				is_width(int i)
{
	if (ft_isdigit(i))
		return (1);
	else
		return (0);
}
