/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 14:03:49 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/14 13:32:09 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strjoin(const char *s1, const char *s2)
{
	char	*dup;
	int		l;
	int		i;
	int		j;
	int		k;

	i = 0;
	l = 0;
	j = ft_strlen((char *)s1);
	k = ft_strlen((char *)s2);
	if ((dup = (char *)malloc(sizeof(char *) * (j + k + 1))) == NULL)
		return (NULL);
	while (s1[i])
	{
		dup[i] = s1[i];
		i++;
	}
	while (s2[l])
	{
		dup[i] = s2[l];
		i++;
		l++;
	}
	dup[i] = '\0';
	return (dup);
}
