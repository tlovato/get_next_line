/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 08:50:39 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/14 13:34:08 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *str, const char *str2)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	k = 0;
	if (str2[i] == '\0')
		return ((char *)&str[i]);
	while (str[i])
	{
		j = i;
		k = 0;
		while (str[j] == str2[k])
		{
			j++;
			k++;
			if (str2[k] == '\0')
				return ((char *)&str[i]);
		}
		i++;
	}
	return (NULL);
}
