/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wide_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 23:15:24 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 23:15:26 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static char				*umask_0(unsigned c)
{
	unsigned char		oc;
	char				*str;

	str = (char *)malloc(sizeof(char) * 2);
	oc = c;
	str[0] = oc;
	str[1] = '\0';
	return (str);
}

static char				*umask_1(unsigned c)
{
	unsigned char		oc;
	char				*str;

	str = (char *)malloc(sizeof(char) * 3);
	oc = (unsigned)(c >> 6) | 0xC0;
	str[0] = oc;
	oc = (unsigned)((c << 26) >> 26) | 0x80;
	str[1] = oc;
	str[2] = '\0';
	return (str);
}

static char				*umask_2(unsigned c)
{
	unsigned char	oc;
	char			*str;

	str = (char *)malloc(sizeof(char) * 3);
	oc = (unsigned)(c >> 12) | 0xE0;
	str[0] = oc;
	oc = (unsigned)((c << 20) >> 26) | 0x80;
	str[1] = oc;
	oc = (unsigned)((c << 26) >> 26) | 0x80;
	str[2] = oc;
	str[3] = '\0';
	return (str);
}

static char				*umask_3(unsigned c)
{
	unsigned char		oc;
	char				*str;

	str = (char *)malloc(sizeof(char) * 4);
	oc = (unsigned)(c >> 18) | 0xF0;
	str[0] = oc;
	oc = (unsigned)((c << 14) >> 26) | 0x80;
	str[1] = oc;
	oc = (unsigned)((c << 20) >> 26) | 0x80;
	str[2] = oc;
	oc = (unsigned)((c << 26) >> 26) | 0x80;
	str[3] = oc;
	str[4] = '\0';
	return (str);
}

char					*w_convert(unsigned int c)
{
	int					bin_len;
	char				*car;

	bin_len = count_len(c);
	if (bin_len <= 7)
		return (car = umask_0(c));
	if (bin_len > 7 && bin_len <= 11)
		return (car = umask_1(c));
	if (bin_len > 11 && bin_len <= 16)
		return (car = umask_2(c));
	else
		return (car = umask_3(c));
}
